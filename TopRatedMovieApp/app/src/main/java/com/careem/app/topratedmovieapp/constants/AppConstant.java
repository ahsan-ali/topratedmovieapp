package com.careem.app.topratedmovieapp.constants;

/**
 * Created by DevAhsan on 22/11/2017.
 */
public final class AppConstant {


    public final static String BASE_URL = "https://api.themoviedb.org/3/";
    public final static String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w185/";
    public final static String API_KEY = "b93ddc3339b3dad50c9e91002a6d680c";

    public final static String TOP_RATED_MOVIE = "movie/top_rated/";

    public final static int MOVIE_GRID_SHOW = 2;

}
