package com.careem.app.topratedmovieapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.careem.app.topratedmovieapp.R;
import com.careem.app.topratedmovieapp.activities.MainActivity;
import com.careem.app.topratedmovieapp.constants.AppConstant;
import com.careem.app.topratedmovieapp.fragments.MovieDetailFragment;
import com.careem.app.topratedmovieapp.models.MovieObject;

import java.util.ArrayList;

/**
 * Created by DevAhsan on 22/11/2017.
 */

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieViewHolder> {

    private ArrayList<MovieObject> dataList;
    Context ctx;


    public MovieListAdapter(Context context, ArrayList<MovieObject> data) {
        //this.dataList = new ArrayList<>();
        //this.dataList.addAll(data);
        this.dataList = data;
        ctx = context;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);

        return new MovieViewHolder(itemView);
    }


    public ArrayList<MovieObject> getList(){
        return this.dataList;
    }

    public void clear(){
        this.dataList.clear();

    }

    public void addAll(ArrayList<MovieObject> data){
        this.dataList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        final MovieObject movie = dataList.get(position);
        holder.title.setText(movie.getTitle()+"");
        holder.desc.setText(movie.getOverview()+"");
        holder.avgVote.setText("Avg Voting: "+movie.getVoteAverage());
        holder.releaseDate.setText("Date : "+movie.getReleaseDate());

        Glide
         .with(ctx)
         .load(AppConstant.IMAGE_BASE_URL+movie.getPosterPath())
         .into(holder.mImage);


        holder.mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MovieDetailFragment currentMovieDetail = new MovieDetailFragment();
                currentMovieDetail.setCurrentMovieObject(movie);
                ((MainActivity)ctx).openFragment(currentMovieDetail, true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class MovieViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, avgVote, releaseDate;
        public ImageView mImage;

        public MovieViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            avgVote = (TextView) view.findViewById(R.id.avgVote);
            releaseDate = (TextView) view.findViewById(R.id.releaseDate);
            mImage = (ImageView) view.findViewById(R.id.mImage);
        }
    }

}
