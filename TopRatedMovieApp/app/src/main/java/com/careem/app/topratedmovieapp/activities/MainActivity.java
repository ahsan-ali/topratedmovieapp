package com.careem.app.topratedmovieapp.activities;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.careem.app.topratedmovieapp.R;
import com.careem.app.topratedmovieapp.fragments.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openFragment(new MainFragment(), false);
    }


    public void openFragment(Fragment newsFragment, boolean isAddToBacstack){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainContainer, newsFragment,newsFragment.getClass().getName());

        if(isAddToBacstack)
            transaction.addToBackStack(null);

        transaction.commit();

    }
}
