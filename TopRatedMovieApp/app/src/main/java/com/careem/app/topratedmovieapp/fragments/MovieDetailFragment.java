package com.careem.app.topratedmovieapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.careem.app.topratedmovieapp.R;
import com.careem.app.topratedmovieapp.constants.AppConstant;
import com.careem.app.topratedmovieapp.models.MovieObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieDetailFragment extends Fragment {

    public TextView title, desc, avgVote;
    public ImageView mImage;
    MovieObject currentMovieObject;

    public MovieDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        title = (TextView) v.findViewById(R.id.title);
        desc = (TextView) v.findViewById(R.id.desc);
        avgVote = (TextView) v.findViewById(R.id.avgVote);
        mImage = (ImageView) v.findViewById(R.id.mImage);

        setMovieDetail();

        return v;
    }

    private void setMovieDetail() {
        title.setText(currentMovieObject.getTitle()+"");
        desc.setText(currentMovieObject.getOverview()+"");
        avgVote.setText("Avg Voting: "+currentMovieObject.getVoteAverage());

        Glide
            .with(getActivity())
            .load(AppConstant.IMAGE_BASE_URL+currentMovieObject.getPosterPath())
            .into(mImage);
    }

    public void setCurrentMovieObject(MovieObject obj) {
        this.currentMovieObject = obj;
    }
}
