package com.careem.app.topratedmovieapp.webHelpers;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by DevAhsan on 22/11/2017.
 */
public class HttpHandler {

    final private static String REQUEST_TYPE = "GET";
    private String connectionUrl;

    public HttpHandler(){
    }

    public HttpHandler(String url ) {
        this.connectionUrl = url;
    }
    public String execute(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
    public String fetchData() throws IOException{
        return this.execute(this.connectionUrl);
    }
    public String execute() throws IOException{
        return this.execute(this.connectionUrl);
    }

}
