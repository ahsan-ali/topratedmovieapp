package com.careem.app.topratedmovieapp.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.careem.app.topratedmovieapp.R;
import com.careem.app.topratedmovieapp.adapters.MovieListAdapter;
import com.careem.app.topratedmovieapp.constants.AppConstant;
import com.careem.app.topratedmovieapp.models.MovieObject;
import com.careem.app.topratedmovieapp.webHelpers.HttpHandler;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    Button sortBtn;
    RecyclerView recyclerMovieList;
    MovieListAdapter mAdapter;
    Context context;
    ProgressDialog mProgressDialog;
    private int currentPage = 0;
    private int totalPage = 0;
    private boolean loading = true;
    RecyclerView.LayoutManager mLayoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loaderShow;



    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context_) {
        super.onAttach(context_);
        context = context_;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        loaderShow = true;
        //Set Progress Dialog
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Loading Movie data....");
        mProgressDialog.setCancelable(false);


        sortBtn = (Button) v.findViewById(R.id.sortBtn);
        recyclerMovieList = (RecyclerView) v.findViewById(R.id.recyclerMovieList);
        mAdapter = new MovieListAdapter(context, new ArrayList<MovieObject>());
        mLayoutManager = new GridLayoutManager(context, AppConstant.MOVIE_GRID_SHOW);
        recyclerMovieList.setLayoutManager(mLayoutManager);
        recyclerMovieList.setItemAnimator(new DefaultItemAnimator());
        recyclerMovieList.setAdapter(mAdapter);

        getMovieDataFromServer(true);

        setRecyclerViewListener();

        sortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortWiseMovieList();
            }
        });

        return v;
    }

    private void sortWiseMovieList() {
       Collections.sort(mAdapter.getList(), new Comparator<MovieObject>() {
            public int compare(MovieObject m1, MovieObject m2) {
                return m1.getReleaseDate().compareTo(m2.getReleaseDate());
            }
        });

        mAdapter.notifyDataSetChanged();
    }

    private void setRecyclerViewListener() {
//        recyclerMovieList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
//                if(dy > 0) //check for scroll down
//                    getMovieDataFromServer(false);
//            }
//        });

        recyclerMovieList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    //Toast.makeText(context,"LAst",Toast.LENGTH_LONG).show();

                    if(currentPage >= totalPage)
                        return;

                    getMovieDataFromServer(false);

                }
            }
        });
    }

    private void getMovieDataFromServer(boolean showLoader) {
        loaderShow = showLoader;
        currentPage+=1;
        if(loaderShow)
            mAdapter.clear();
        new GetMoviesAsyncTask().execute();
    }

    private class GetMoviesAsyncTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //if (loaderShow)
                mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String baseUrl = AppConstant.BASE_URL;
            String url = baseUrl + AppConstant.TOP_RATED_MOVIE + "?api_key="+ AppConstant.API_KEY + "&page="+currentPage;

            HttpHandler movieDbHandler = new HttpHandler(url);
            try {
                String jsonData = movieDbHandler.fetchData();
                return jsonData;
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (response != null){
                ArrayList<MovieObject> data = new ArrayList<>();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    totalPage = jsonObject.getInt("total_pages");
                    JSONArray resultArray = jsonObject.getJSONArray("results");
                    if(resultArray.length() > 0){
                        for (int index = 0; index < resultArray.length(); index++) {
                            data.add(new Gson().fromJson(resultArray.get(index).toString(), MovieObject.class));
                        }
                    }
                    mAdapter.addAll(data);
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
            }
        }
    }








}
